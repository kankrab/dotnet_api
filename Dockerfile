   FROM microsoft/dotnet:2.2-sdk AS build-env
   WORKDIR /app
   COPY . /app
   RUN dotnet publish -o out


   FROM microsoft/dotnet:2.2-aspnetcore-runtime
   WORKDIR /app
   COPY --from=build-env /app/out .
   ENTRYPOINT ["dotnet", "dotNetCore_for_pipeline.dll"]
